# Activity Pub for Go

[![MIT Licensed](https://img.shields.io/github/license/mariusor/activitypub.go.svg)](https://raw.githubusercontent.com/mariusor/activitypub.go/master/LICENSE)
[![Build status](https://img.shields.io/travis/mariusor/activitypub.go.svg)](https://travis-ci.org/mariusor/activitypub.go)
[![Test Coverage](https://img.shields.io/codecov/c/github/mariusor/activitypub.go.svg)](https://codecov.io/gh/mariusor/activitypub.go)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/29664f7ae6c643bca76700143e912cd3)](https://www.codacy.com/app/mariusor/activitypub.go/dashboard)
<!-- [![Go Report Card](https://goreportcard.com/badge/github.com/mariusor/activitypub.go)](https://goreportcard.com/report/github.com/mariusor/activitypub.go) -->

Basic lib for using [activity pub](https://www.w3.org/TR/activitypub/#Overview) API in Go.

Please see issue [#1](https://github.com/mariusor/activitypub.go/issues/1) before considering this project for real life applications.

## Usage

```go
import "github.com/mariusor/activitypub.go/activitypub"
```
